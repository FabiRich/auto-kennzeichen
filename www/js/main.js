var input_element = null;
var result_element = null;
var kennzeichen_obj = null;


/* Init all variables */
function init()
{
    'use strict';
    input_element = document.getElementById("search_input");
    result_element = document.getElementById("result");

    input_element.addEventListener('keyup', search);
    kennzeichen_obj = JSON.parse(kennzeichen);

    //Set Focus
    input_element.focus();

    try
    {
        Keyboard.show();
    }
    catch (err) {}

    //Set Focus
    input_element.focus();
}

/* Search for User Input */
function search(event)
{
    'use strict';
    var search_term_up = input_element.value.trim().toUpperCase();

    if(search_term_up == "")
    {
        if(result_element != null)
        {
            result_element.innerHTML = "";
        }
        return;
    }

    var add_string = "";

    for(var key in kennzeichen_obj)
    {
        if(kennzeichen_obj.hasOwnProperty(key))
        {
            if(key.startsWith(search_term_up))
            {
                add_string = add_string + "<li><strong>" + key + "</strong><span>" + kennzeichen_obj[key].title + " -  <i>" + kennzeichen_obj[key].state + "</i></span></li>";
            }
        }
    }

    //No Results? => Revere
    if(add_string == "")
    {
        for(var key_r in kennzeichen_obj)
        {
            if(kennzeichen_obj.hasOwnProperty(key_r))
            {
                var current = kennzeichen_obj[key_r].title.toUpperCase();

                if(current.indexOf(search_term_up) !== -1)
                {
                    add_string = add_string + "<li><strong>" + key_r + "</strong><span>" + kennzeichen_obj[key_r].title + " - <i>" + kennzeichen_obj[key_r].state + "</i></span></li>";
                }
            }
        }
    }

    if(add_string == "")
    {
        add_string = "<li> Keine Ergebnisse!</li>";
    }

    if(result_element != null)
    {
        result_element.innerHTML = add_string;
    }
}

/* Show infos */
function show_info()
{
    try
    {
        document.getElementById("info_icon").style.display = "none";
        document.getElementById("back_icon").style.display = "block";
        document.getElementById("main_div").style.display = "none";
        document.getElementById("info_div").style.display = "flex";
    }
    catch (err) {}
}

/* Back */
function info_back()
{
    try
    {
        document.getElementById("info_icon").style.display = "block";
        document.getElementById("back_icon").style.display = "none";
        document.getElementById("main_div").style.display = "flex";
        document.getElementById("info_div").style.display = "none";
    }
    catch (err) {}
}

/* DOM is loaded */
function onLoad()
{
    'use strict';
    document.addEventListener("deviceready", onDeviceReady, false);
}

/* Cordova is loaded */
function onDeviceReady()
{
    'use strict';
    init();

    document.addEventListener("backbutton", function(e)
    {
        e.preventDefault();

        if(window.getComputedStyle(document.getElementById("main_div")).display == "flex")
        {
            try
            {
                navigator.app.exitApp();
            }
            catch (err) {}
        }
        else
        {
            info_back();
        }

        return;

    }, false);

}
