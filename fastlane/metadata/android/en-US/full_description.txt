Simple app to tell by a car's number plate where the car is registered. If you're using Android's TalkBack functionality, a connection to Google servers will be established.
